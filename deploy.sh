#!/bin/bash

pytest -p no:cacheprovider /etc/nginx/tests
res=$?

if [ $res -ne 0 ]; then
 exit 125
else
 echo "Passed"
fi

/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf