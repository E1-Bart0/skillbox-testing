def test_os_os_alpine(host):
    release_file = host.file("/etc/os-release")
    assert release_file.contains("Alpine")
