import pytest


@pytest.mark.parametrize(
    ("pkg_name", "pkg_version"), [
        ("nginx", "1.16.1")
    ])
def test__packages_is_installed(host, pkg_name, pkg_version):
    package = host.package(pkg_name)
    assert package.is_installed
    assert package.version.startswith(pkg_version)
